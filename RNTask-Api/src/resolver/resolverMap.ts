import { IResolvers } from 'graphql-tools'
import axios from 'axios';
import { ApolloError } from 'apollo-server-core';

// Url and routes
const base_url = 'https://www.metaweather.com/api'
const woeid_route = base_url + '/location/search/?lattlong='
const weather_route = base_url + '/location/'

// Resolver
const resolverMap: IResolvers = {
    Query: {
        // Get woeid list from: lat, lon
        async getWoeid(_, args) {
            const url = woeid_route + args.lat + ',' + args.lon
            return await axios.get(url).then((res) => {
                // return 5 cities including the user city
                return res && res.data && res.data.length >= 5 ? res.data.slice(0, 5) : null
            }).catch((err) => {
                return new ApolloError(err)
            })
        },
        // Get weather object from: woeid, year, month, day
        async getWeather(_, args) {
            const url = weather_route + args.woeid + '/' + args.year + '/' + args.month + '/' + args.day + '/'
            return await axios.get(url).then((res) => {
                return res && res.data && res.data && res.data.length > 0 ? res.data[0] : null
            }).catch((err) => {
                return new ApolloError(err)
            })
        }
    }
}
export default resolverMap