import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import depthLimit from 'graphql-depth-limit'
import { createServer } from 'http'
import compression from 'compression'
import cors from 'cors'
import schema from './schema'

// Apollo server and express app
const app = express()
const server = new ApolloServer({
    schema,
    validationRules: [depthLimit(10)],
})

app.use('*', cors()) // Avoid issues with API calls
app.use(compression()) // Compress all responses
server.applyMiddleware({ app, path: '/graphql' })

const httpServer = createServer(app)
const port = 4000
httpServer.listen({ port: port }, () => console.log('\n🚀  RNTask API is now running on http://localhost:' + port + '/graphql'))