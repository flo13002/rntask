
  

# Project Documentation

This is the documentation for the RNTask project.

## React Native application

### Technologies

React Native, Expo (32.0)

### Installation

If node_modules/ is not in the main directory, execute:

```
npm install
```
If you don't have the file Secret.js in ~/RNTask/communication/Secret.js, that's because the file was not pushed for security. 
If it's the case, then execute (in RNTask/):
```
cp -fr [path_to_the_file]/Secret.js ./communication/
```

Then, execute this command to launch the project. It will open a new tab in your brower and you'll be able to launch the app from it using "Run on iOS simulator" or "Run on Android simulator":

```
expo start
```

### Run the application

#### Run on a physical device

Download the Expo client app from the AppStore or Google Play Store.
When the project is started using expo start, you should be able to see the project in the Home page of the Expo client application.
Click on it, it will automatically launch the app from it.

#### Run on a simulator

When executing expo start, it will automatically open a new tab in your brower and you'll be able to launch the app from it using "Run on iOS simulator" or "Run on Android simulator".

### Project architecture

#### Communication:

Communication methods between the react native application and the API (async). 
Secret.js handles secret data like the API url and routes, etc. (Not present in this repository for security)
  
#### Config:

All the generic data like fonts, colors, strings (possibility of translations), timings, etc.

#### Routes:

All the routes (navigation) of the application.

#### Components/main:

Main pages of the application.

#### Components/common:

Generic components used in the pages (slider, loading, etc).

### Specifications

- It works on iOS and Android
- The app handles orientation change
- It uses material design for icons (vector icons)
- The app is responsive on phones and tablets

### Production

To generate the .ipa file (iOS build), you'll need to execute this command:

```
expo build:ios
```
To generate the .apk (Android build), execute:

```
expo build:android
```

It will generate everything for you (certificates, etc) and you'll be able to download the builds from your browser. For more informations about this process, visit: [https://docs.expo.io/versions/latest/distribution/building-standalone-apps/](https://docs.expo.io/versions/latest/distribution/building-standalone-apps/).

## API 

### Technologies

Typescript, Node.js Express, Apollo Server, GraphQL

### Installation

If node_modules/ is not in the main directory, execute:

```
npm install
```
To compile the API or if you make some change, execute:

```
npm run build
```

Then, to launch the API, execute:

```
npm start
```
### Development environment

To test the requests locally, you can use the graphql tool on your browser (after launching the API locally using npm start):
```
http://localhost:4000/graphql
```
You'll then be able to fetch the API locally using GraphQL. Here's an example:
```
query {
  getWoeid(lat: "51.5074", lon: "0.1278") {
	    woeid,
	    title,
	    location_type,
	    latt_long,
	    distance,
	}
}
```

### Production

To launch the API on production mode with restart option, execute this command in the API directory:

```
pm2 start dist/server.js --watch
```

Don't forget to run this command if you made some changes before using pm2:

```
npm run build
```

## Version

```
1.0.0
```