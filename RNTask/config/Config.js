import { PureComponent } from 'react'

class Config extends PureComponent {
    constructor(props) {
        super(props)
        // Delays
        this.transitionDelay = 500
        this.splashDelay = 1000
        // Colors
        this.backgroundColor = '#ffffff'
        this.mainColor = '#756BF5'
        this.mainColorDarker = '#4d42d6'
        this.fontColor = '#2d3436'
        this.lightGray = '#eaecee'
        // Fonts
        this.fontBold = 'Montserrat-Bold'
        this.fontRegular = 'Montserrat-Regular'
        this.fontLight = 'Montserrat-Light'
        // Strings
        this.noLocationTitle = 'No access to your location...'
        this.noLocationSubtitle = 'The app needs to access your location in order to provide the weather informations. Enable the access in your settings to see the weather.'
        this.noInternetTitle = 'No internet connection...'
        this.noInternetSubtitle = 'Check your internet connection. The app needs an internet connection to be able to get the weather informations.'
        this.errorTitle = 'Something went wrong...'
        this.errorSubtitle = 'Something went wrong while fetching the data from our server. Please try again in few seconds.'
    }

    // DELAYS
    // Get transition delay
    getTransitionDelay() {
        return this.transitionDelay
    }

    // Get splash delay
    getSplashDelay() {
        return this.splashDelay
    }



    // COLORS
    // Get background color
    getBackgroundColor() {
        return this.backgroundColor
    }

    // Get main color
    getMainColor() {
        return this.mainColor
    }

    // Get main color darker
    getMainColorDarker() {
        return this.mainColorDarker
    }

    // Get font color
    getFontColor() {
        return this.fontColor
    }

    // Get color light gray
    getLightGrayColor() {
        return this.lightGray
    }



    // FONTS
    // Get font regular
    getFontRegular() {
        return this.fontRegular
    }

    // Get font bold
    getFontBold() {
        return this.fontBold
    }

    // Get font light
    getFontLight() {
        return this.fontLight
    }



    // STRINGS
    // Get no location title
    getNoLocationTitle() {
        return this.noLocationTitle
    }

    // Get no location subtitle
    getNoLocationSubtitle() {
        return this.noLocationSubtitle
    }

    // Get no internet title
    getNoInternetTitle() {
        return this.noInternetTitle
    }

    // Get no internet subtitle
    getNoInternetSubtitle() {
        return this.noInternetSubtitle
    }

    // Get error title
    getErrorTitle() {
        return this.errorTitle
    }

    // Get error subtitle
    getErrorSubtitle() {
        return this.errorSubtitle
    }
}

const config = new Config()

export default (config)