import React, { Component } from 'react'
import { View, StatusBar, StyleSheet } from 'react-native'
import { AppLoading, SplashScreen } from 'expo'
import Routes from './routes/Routes'
import Config from './config/Config'

export default class App extends Component {
  constructor(props) {
    super(props)
    console.disableYellowBox = true
    this.state = {
      isAppReady: false,
    }
  }

  // Load fonts during splash screen
  async _cacheSplashResourcesAsync() {
    await Expo.Font.loadAsync({
      'Montserrat-Bold': require('./assets/fonts/Montserrat-Bold.otf'),
      'Montserrat-Light': require('./assets/fonts/Montserrat-Light.otf'),
      'Montserrat-Regular': require('./assets/fonts/Montserrat-Regular.otf'),
      'Montserrat-SemiBold': require('./assets/fonts/Montserrat-SemiBold.otf'),
    })
  }

  // Hide splash screen after splash delay 
  hideSplash() {
    setTimeout(() => {
      this.setState({ isAppReady: true })
      SplashScreen.hide()
    }, Config.getSplashDelay())
  }

  render() {
    if (!this.state.isAppReady) {
      return (
        <AppLoading
          startAsync={this._cacheSplashResourcesAsync.bind(this)}
          onFinish={() => this.hideSplash()}
          autoHideSplash={false}
        />
      )
    }
    else {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor={'transparent'} barStyle="dark-content" translucent={true} />
          <Routes />
        </View>
      )
    }
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%'
  }
})