import { createStackNavigator, createAppContainer } from 'react-navigation'
import Weather from '../components/main/Weather'

// Set routes in main container (can add other routes/stacks here)
const mainContainer = createStackNavigator({
    Weather: { screen: Weather, navigationOptions: ({ navigation }) => ({ swipeEnabled: false, gesturesEnabled: false }) },
}, {
        initialRouteName: 'Weather',
        headerMode: 'hidden',
        cardStyle: {
            backgroundColor: 'transparent',
            shadowOpacity: 0,
        },
    })

// Create container with routes 
const Routes = createAppContainer(mainContainer)

export default Routes