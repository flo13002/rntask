import React, { Component } from 'react'
import { StyleSheet, View, Dimensions, Platform, StatusBar, Text } from 'react-native'
import { Location, Permissions, LinearGradient, Constants } from 'expo';
import { SafeAreaView } from 'react-navigation'
import Communication from '../../communication/Communication'
import Config from '../../config/Config'
import Loading from '../common/Loading'
import Slider from '../common/Slider'

export default class Weather extends Component {
  constructor(props) {
    super(props)
    this.state = {
      error: false,
      noLocation: false,
      connectivity: true,
      loading: true,
      statusBVisible: Dimensions.get('window').width > Dimensions.get('window').height ? false : true,
      cities: [],
    }
  }

  // Fetch woeid list (cities) when view is mounted and listen to orientation
  componentDidMount() {
    this.checkConnectivity()
    this.listenToOrientation()
  }

  // Listen to orientation and hide status bar if landscape
  listenToOrientation() {
    Dimensions.addEventListener('change', () => {
      const newStatus = Dimensions.get('window').width > Dimensions.get('window').height ? false : true
      if (this.state.statusBVisible !== newStatus) {
        this.setState({ statusBVisible: newStatus })
      }
    })
  }

  // Check internet connectivity
  checkConnectivity() {
    Communication.checkConnectivity().then((connectionInfo) => {
      if (connectionInfo.type === 'wifi' || connectionInfo.type === 'cellular') {
        this.getWoeid()
      }
      else {
        // No internet connectivity - display error
        this.setState({ connectivity: false })
      }
    })
  }

  // Get location of user
  async getUserCoordinates() {
    let { status } = await Permissions.getAsync(Permissions.LOCATION)
    if (status !== "granted") {
      status = await Permissions.askAsync(Permissions.LOCATION)
      if (status.status !== 'granted') {
        return null
      }
    }
    if (!Constants.isDevice && Platform.OS === 'android') {
      // No location on android emulator, return fixed position (London)
      return { coords: { latitude: '51.5074', longitude: '0.1278' } }
    }
    else {
      return await Location.getCurrentPositionAsync({})
    }
  }

  // Get woeid list (cities) from API
  async getWoeid() {
    const location = await this.getUserCoordinates()
    if (location) {
      Communication.getWoeid(location.coords.latitude, location.coords.longitude).then((res) => {
        if (res && res !== undefined && res.data && res.data.getWoeid && res.data.getWoeid.length > 0) {
          this.setState({ cities: res.data.getWoeid, loading: false })
        }
        else {
          // Error while fetching woeid data - display error
          this.setState({ error: true, loading: false })
        }
      })
    }
    else {
      // No access to location - display error
      this.setState({ noLocation: true, loading: false })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <LinearGradient style={styles.container}
          colors={[Config.getMainColor(), Config.getMainColorDarker()]}
          start={{ x: 0, y: .1 }} end={{ x: .1, y: 1 }}>
          {this.state.statusBVisible ? <StatusBar hidden={false} /> : (Platform.OS === 'ios' ? <StatusBar hidden={true} /> : null)}
          {this.state.cities.length > 0 && !this.state.loading ?
            <View>
              <Slider cities={this.state.cities} />
            </View > : null}
          {!this.state.connectivity && !this.state.loading ?
            <SafeAreaView style={styles.safearea} forceInset={{ left: 'always', right: 'always', bottom: 'never' }}>
              <View style={styles.errorContainer}>
                <Text style={styles.title} color={Config.getFontBold()}>{Config.getNoInternetTitle()}</Text>
                <Text style={styles.subtitle} color={Config.getFontBold()}>{Config.getNoInternetSubtitle()}</Text>
              </View>
            </SafeAreaView>
            : null}
          {this.state.noLocation && this.state.connectivity && !this.state.loading ?
            <SafeAreaView style={styles.safearea} forceInset={{ left: 'always', right: 'always', bottom: 'never' }}>
              <View style={styles.errorContainer}>
                <Text style={styles.title} color={Config.getFontBold()}>{Config.getNoLocationTitle()}</Text>
                <Text style={styles.subtitle} color={Config.getFontBold()}>{Config.getNoLocationSubtitle()}</Text>
              </View>
            </SafeAreaView>
            : null}
          {this.state.error && !this.state.noLocation && this.state.connectivity && !this.state.loading ?
            <SafeAreaView style={styles.safearea} forceInset={{ left: 'always', right: 'always', bottom: 'never' }}>
              <View style={styles.errorContainer}>
                <Text style={styles.title} color={Config.getFontBold()}>{Config.getErrorTitle()}</Text>
                <Text style={styles.subtitle} color={Config.getFontBold()}>{Config.getErrorSubtitle()}</Text>
              </View>
            </SafeAreaView>
            : null}
          {this.state.loading ? <View style={styles.loading}><Loading color={Config.getBackgroundColor() + '50'} /></View> : null}
        </LinearGradient>
      </View>
    )
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: Config.getBackgroundColor()
  },
  safearea: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  errorContainer: {
    height: '100%',
    marginLeft: 30,
    marginRight: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 18,
    fontFamily: Config.getFontBold(),
    marginBottom: 5,
    color: Config.getBackgroundColor()
  },
  subtitle: {
    fontSize: 15,
    fontFamily: Config.getFontLight(),
    color: Config.getBackgroundColor() + '70'
  },
  loading: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  }
})