import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import SliderItem from '../common/SliderItem'

export default class Slider extends Component {
    constructor(props) {
        super(props)
    }

    // Render slider items (pages in the slider)
    renderItem = props => (
        <SliderItem item={props} />
    )

    render() {
        return (
            <View style={styles.container}>
                <AppIntroSlider
                    slides={this.props.cities}
                    renderItem={this.renderItem}
                    keyExtractor={(item) => item.woeid.toString()}
                    showSkipButton={false}
                    showPrevButton={false}
                    showNextButton={false}
                    showDoneButton={false}
                    maxToRenderPerBatch={1}
                />
            </View>
        )
    }
}

// Styles
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
})
