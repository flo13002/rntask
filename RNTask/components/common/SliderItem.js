import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, Dimensions, ScrollView, FlatList } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Config from '../../config/Config'
import Communication from '../../communication/Communication'
import Loading from './Loading'
import SliderRowItem from './SliderRowItem'

export default class SliderItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            weatherList: { sn: 'weather-snowy', sl: 'weather-hail', h: 'weather-hail', t: 'weather-lightning', hr: 'weather-pouring', lr: 'weather-rainy', s: 'weather-rainy', hc: 'weather-fog', lc: 'weather-partlycloudy', c: 'weather-sunny' },
            weather: null,
            loading: true,
            error: false,
        }
        this.rows = []
    }

    // Get data from weather using woeid data
    componentDidMount() {
        const date = this.getCurrentDate()
        this.getWeather(this.props.item.woeid, date)
    }

    // Get day,month,year from current date
    getCurrentDate() {
        const today = new Date()
        const date = { day: today.getDate(), month: parseInt(today.getMonth() + 1), year: today.getFullYear() }
        return date
    }

    // Get weather data from API
    async getWeather(woeid, date) {
        Communication.getWeather(woeid, date).then((res) => {
            if (res && res !== undefined && res.data && res.data.getWeather) {
                this.setState({ weather: res.data.getWeather, loading: false })
            }
            else {
                // Error while fetching weather data - display error
                this.setState({ error: true, loading: false })
            }
        })
    }

    render() {
        // Add rows to this.rows, depending on the weather object
        if (!this.state.loading && this.state.weather) {
            this.rows = [{ id: '0', text: 'Min. temperature : ' + parseInt(this.state.weather.min_temp) + '°C', icon: 'minus', type: 'Entypo' },
            { id: '1', text: 'Max. temperature : ' + parseInt(this.state.weather.max_temp) + '°C', icon: 'plus', type: 'Entypo' },
            { id: '2', text: 'Air pressure : ' + parseInt(this.state.weather.air_pressure) + ' Mbar', icon: 'air', type: 'Entypo' },
            { id: '3', text: 'Wind speed : ' + parseInt(this.state.weather.wind_speed) + ' Mph', icon: 'speedometer', type: 'MaterialCommunityIcons' },
            { id: '4', text: 'Wind direction : ' + parseInt(this.state.weather.wind_direction) + (parseInt(this.state.weather.wind_direction) > 1 ? ' Degrees' : 'Degree'), icon: 'wind', type: 'Feather' },
            { id: '5', text: 'Visibility : ' + parseInt(this.state.weather.visibility) + (parseInt(this.state.weather.visibility) > 1 ? ' Miles' : 'Mile'), icon: 'eye', type: 'SimpleLineIcons' },
            { id: '6', text: 'Humidity : ' + this.state.weather.humidity + ' %', icon: 'drop', type: 'Entypo' }
            ]
        }
        return (
            <View style={styles.container}>
                {!this.state.error && !this.state.loading && this.state.weather ?
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false} overScrollMode={'never'}>
                            <View style={styles.content}>
                                <View style={[styles.weatherLogo, { width: Dimensions.get('window').height / 5, height: Dimensions.get('window').height / 5 }]}>
                                    <MaterialCommunityIcons name={this.state.weatherList[this.state.weather.weather_state_abbr]} size={Dimensions.get('window').height / 5} color={Config.getBackgroundColor() + '20'} />
                                </View>
                                <View style={{ width: Dimensions.get('window').width - (Dimensions.get('window').width / 5) }}>
                                    <Text style={styles.city}>{this.props.item.title}</Text>
                                    <Text style={styles.temperature}>{parseInt(this.state.weather.the_temp) + '°C'}</Text>
                                    <Text style={styles.time}>{this.state.weather.weather_state_name}</Text>
                                    <View style={styles.separator} />
                                    <FlatList
                                        style={styles.flat}
                                        data={this.rows}
                                        renderItem={({ item }) => <SliderRowItem item={item} />}
                                        keyExtractor={(item) => item.id}
                                        extraData={this.rows}
                                        maxToRenderPerBatch={1}
                                    />
                                    <View style={{ marginBottom: 80 }} />
                                </View>
                            </View>
                        </ScrollView>
                    </View> : null}
                {this.state.error && !this.state.loading ?
                    <SafeAreaView style={styles.safearea} forceInset={{ left: 'always', right: 'always', bottom: 'never' }}>
                        <View style={styles.errorContainer}>
                            <Text style={styles.title} color={Config.getFontBold()}>{Config.getErrorTitle()}</Text>
                            <Text style={styles.subtitle} color={Config.getFontBold()}>{Config.getErrorSubtitle()}</Text>
                        </View>
                    </SafeAreaView>
                    : null}
                {this.state.loading ? <View style={styles.loading}><Loading color={Config.getBackgroundColor() + '50'} /></View> : null}
            </View>
        )
    }
}

// Styles
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    safeContainer: {
        flex: 1,
        backgroundColor: 'red'
    },
    safearea: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    content: {
        alignItems: 'center',
        marginTop: 30,
    },
    errorContainer: {
        height: '100%',
        marginLeft: 30,
        marginRight: 30,
        justifyContent: 'center',
    },
    weatherLogo: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    title: {
        fontSize: 18,
        fontFamily: Config.getFontBold(),
        marginBottom: 5,
        color: Config.getBackgroundColor()
    },
    subtitle: {
        fontSize: 15,
        fontFamily: Config.getFontLight(),
        color: Config.getBackgroundColor() + '70'
    },
    city: {
        fontFamily: Config.getFontBold(),
        color: Config.getBackgroundColor(),
        fontSize: 25,
        textAlign: 'center',
        marginBottom: 15,
    },
    temperature: {
        fontFamily: Config.getFontRegular(),
        color: Config.getBackgroundColor(),
        //color: '#756BF5',
        fontSize: 40,
        textAlign: 'center',
        marginBottom: 15,
    },
    time: {
        fontFamily: Config.getFontLight(),
        color: Config.getBackgroundColor(),
        fontSize: 15,
        textAlign: 'center',
    },
    flat: {
        marginBottom: 10
    },
    loading: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    separator: {
        height: 2,
        backgroundColor: Config.getBackgroundColor() + '20',
        marginTop: 30,
        marginBottom: 20
    }
})
