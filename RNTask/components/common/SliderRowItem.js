import React, { PureComponent } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { MaterialCommunityIcons, SimpleLineIcons, Entypo, Feather } from '@expo/vector-icons'
import Config from '../../config/Config'

export default class SliderRowItem extends PureComponent {
    render() {
        return (
            <View>
                <View style={styles.row}>
                    <View style={styles.icon}>
                        {this.props.item.type === 'Entypo' ? <Entypo name={this.props.item.icon} size={25} color={Config.getBackgroundColor()} /> :
                            (this.props.item.type === 'MaterialCommunityIcons' ? <MaterialCommunityIcons name={this.props.item.icon} size={25} color={Config.getBackgroundColor()} /> :
                                ((this.props.item.type === 'Feather' ? <Feather name={this.props.item.icon} size={25} color={Config.getBackgroundColor()} /> :
                                    <SimpleLineIcons name={this.props.item.icon} size={25} color={Config.getBackgroundColor()} />)))}
                    </View>
                    <Text style={styles.rowText}>{this.props.item.text}</Text>
                </View>
                <View style={styles.separator} />
            </View>
        )
    }
}

// Styles
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 20,
    },
    icon: {
        width: 25,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowText: {
        marginLeft: 15,
        fontSize: 15,
        fontFamily: Config.getFontLight(),
        color: Config.getBackgroundColor()
    },
    separator: {
        height: 2,
        backgroundColor: Config.getBackgroundColor() + '20',
        marginBottom: 20
    },
})
