import React, { PureComponent } from 'react'
import { StyleSheet, View } from 'react-native'
import { DotIndicator } from 'react-native-indicators'

export default class Loading extends PureComponent {
    render() {
        return (
            <View style={styles.mainContainer}>
                <DotIndicator color={this.props.color} count={3} size={10} />
            </View >
        )
    }
}

// Styles
const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: '100%',
    },
})