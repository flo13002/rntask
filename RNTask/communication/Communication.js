import { PureComponent } from 'react'
import { NetInfo } from 'react-native'
import Secret from './Secret'

class Communication extends PureComponent {
    constructor(props) {
        super(props)
        // Set url of API root
        this.url = Secret.getUrlRoot()
        // Get routes
        this.routeWoeid = Secret.getRouteWoeid()
        this.routeWeather = Secret.getRouteWeather()
    }

    // Check internet connection
    async checkConnectivity() {
        return await NetInfo.getConnectionInfo()
    }

    // Get woeid list from API
    async getWoeid(lat, lon) {
        const query = `{
            getWoeid(lat: "${lat}", lon: "${lon}") { 
                woeid,
                title
            } 
        }`
        return await this.sendQueryPOST(this.routeWoeid, JSON.stringify({ query }))
    }

    // Get weather object from API
    async getWeather(woeid, date) {
        const query = `{ 
            getWeather(woeid: "${woeid}",  year: "${date.year}", month: "${date.month}", day: "${date.day}") { 
                id,
                weather_state_name,
                weather_state_abbr,
                min_temp,
                max_temp,
                the_temp,
                wind_speed,
                wind_direction,
                air_pressure,
                humidity,
                visibility,
            }
        }`
        return await this.sendQueryPOST(this.routeWeather, JSON.stringify({ query }))
    }

    // Send query POST
    async sendQueryPOST(endPoint, body) {
        const headerPayload = {
            'Content-Type': 'application/json',
        }
        const url = this.url + endPoint
        return await fetch(url,
            {
                method: 'POST',
                body: body,
                headers: headerPayload,
                timeout: 10000
            })
            .then((response) => response.json())
            .catch((error) => error.json())
    }
}

const com = new Communication()
export default (com)